from numpy import dot, matrix, sum

class Perceptron():
  def __init__(self, inputs, weights, bias):
    self.bias = bias
    self.inputs = inputs
    self.weights = weights
    
  def predict(self):
    matrix_product = dot(self.inputs, self.weights)
    sum_matrix = matrix_product.sum()
    if (sum_matrix + self.bias >= 0):
      return 1
    else:
      return 0

if (__name__ == '__main__'):
  perceptron = Perceptron([[6, 3, 2]], [[1], [0], [0]], -7)
  print perceptron.predict()

